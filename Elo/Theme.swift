//
//  Theme.swift
//  HopShop
//
//  Created by apple on 03/07/20.
//  Copyright © 2020 apple. All rights reserved.
//

import Foundation


struct Sizing {

    /// Size `0`
    public static let none: Int = 0

    /// Size `2`
    public static let xxs: Int = 2

    /// Size `4`
    public static let xs: Int = 4

    /// Size `8`
    public static let s: Int = 8

    /// Size `16`
    public static let m: Int = 16

    /// Size `24`
    public static let l: Int = 24

    /// Size `32`
    public static let xl: Int = 32

    /// Size `40`
    public static let xxl: Int = 40

    /// Size `48`
    public static let xxxl: Int = 48

    /// Size `56`
    public static let jumbo: Int = 56

    /// Size `64`
    public static let xjumbo: Int = 64
}
