//
//  UIColor+Hopshop.swift
//  HopShop
//
//  Created by apple on 03/07/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit
import SnapKit

extension UIColor {
    
    
    convenience init?(hex: String) {
        var hexSanitized = hex.trimmingCharacters(in: .whitespacesAndNewlines)
        hexSanitized = hexSanitized.replacingOccurrences(of: "#", with: "")

        var rgb: UInt32 = 0

        var r: CGFloat = 0.0
        var g: CGFloat = 0.0
        var b: CGFloat = 0.0
        var a: CGFloat = 1.0

        let length = hexSanitized.count

        guard Scanner(string: hexSanitized).scanHexInt32(&rgb) else { return nil }

        if length == 6 {
            r = CGFloat((rgb & 0xFF0000) >> 16) / 255.0
            g = CGFloat((rgb & 0x00FF00) >> 8) / 255.0
            b = CGFloat(rgb & 0x0000FF) / 255.0

        } else if length == 8 {
            r = CGFloat((rgb & 0xFF000000) >> 24) / 255.0
            g = CGFloat((rgb & 0x00FF0000) >> 16) / 255.0
            b = CGFloat((rgb & 0x0000FF00) >> 8) / 255.0
            a = CGFloat(rgb & 0x000000FF) / 255.0

        } else {
            return nil
        }

        self.init(red: r, green: g, blue: b, alpha: a)
    }
    
    static var blueTheme: UIColor {
        
        return UIColor(hex: "#094275") ?? UIColor.white
    }
    
    static var OrangeTheme: UIColor {
        
        return UIColor(hex: "#dc7364") ?? UIColor.white
    }
    
    static var whiteTheme: UIColor {
        
        return UIColor(hex: "#ffffff") ?? UIColor.white
    }
}


extension UIViewController {

    var bottomLayoutGuideConstraint: SnapKit.ConstraintItem {

        return self.view.safeAreaLayoutGuide.snp.bottom
    }

    var topLayoutGuideConstraint: SnapKit.ConstraintItem {

        return self.view.safeAreaLayoutGuide.snp.top
    }

}
