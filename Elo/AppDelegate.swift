//
//  AppDelegate.swift
//  Elo
//
//  Created by apple on 07/09/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

import Firebase
import GoogleSignIn

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication
        , didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        FirebaseApp.configure()

        let frame = UIScreen.main.bounds
        window = UIWindow(frame: frame)
        window!.rootViewController = UIViewController()
        window!.makeKeyAndVisible()

        self.configure()
        return true
    }

    @available(iOS 9.0, *)
    func application(_ app: UIApplication
        , open url: URL
        , options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {

        return GIDSignIn.sharedInstance().handle(url)
    }

    func configure() {

        let navigationBarAppearace = UINavigationBar.appearance()
        navigationBarAppearace.tintColor = .whiteTheme
        navigationBarAppearace.barTintColor = .blueTheme
        navigationBarAppearace.isTranslucent = false

        let tabBarAppearace = UITabBar.appearance()
        tabBarAppearace.tintColor = .whiteTheme
        tabBarAppearace.barTintColor = .blueTheme
        tabBarAppearace.isTranslucent = false
    }
}
